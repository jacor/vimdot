" Use vim settings, rather then vi settings (much better!)
" This must be first, because it changes other options as a side effect.
set nocompatible

" Use pathogen to easily modify the runtime path to include all plugins under
" the ~/.vim/bundle directory
filetype off                    " force reloading *after* pathogen loaded
execute pathogen#infect()
filetype plugin indent on       " enable detection, plugins and indenting in one step

" Editing behaviour {{{
set noshowmode     " Don't show current mode, defer to airline
set timeoutlen=750 " Minimize the pause when exiting insert mode
set nowrap         " don't wrap lines
set tabstop=2      " a tab is two spaces
set softtabstop=2  " when hitting <BS>, pretend like a tab is removed, even if spaces
set expandtab      " expand tabs by default (overloadable per file type later)
set shiftwidth=2   " number of spaces to use for autoindenting
set shiftround     " use multiple of shiftwidth when indenting with '<' and '>'
set backspace=indent,eol,start " allow backspacing over everything in insert mode
set autoindent     " always set autoindenting on
set copyindent     " copy the previous indentation on autoindenting
"set number         " always show line numbers
set showmatch      " set show matching parenthesis
set ignorecase     " ignore case when searching
set smartcase      " ignore case if search pattern is all lowercase, case-sensitive otherwise
set smarttab       " insert tabs on the start of a line according to shiftwidth, not tabstop
set scrolloff=2    " keep 2 lines off the edges of the screen when scrolling
set hlsearch       " highlight search terms
set incsearch      " show search matches as you type
set gdefault       " search/replace "globally" (on a line) by default
set listchars=tab:▸\ ,trail:·,extends:#,nbsp:·
set nolist         " don't show invisible characters by default,
                   " but it is enabled for some file types (see later)
set fileformats="unix,dos,mac"
set formatoptions+=1    "  When wrapping paragraphs, don't end lines
                        "    with 1-letter words (looks stupid)

" See http://stevelosh.com/blog/2010/09/coming-home-to-vim
nnoremap / /\v
vnoremap / /\v

" See http://nvie.com/posts/how-i-boosted-my-vim/
set pastetoggle=<F2>   " when in insert mode, ready to paste, press <F2>,
                       " Vim will switch to paste mode, disabling automatic
                       " features. To re-enable, type <F2> again.
" Use Q for formatting the current paragraph (or selection)
vnoremap Q gq
nnoremap Q gqap
" }}}

" Editor layout {{{
set termencoding=utf-8
set encoding=utf-8
set lazyredraw                  " don't update the display while executing macros
set laststatus=2                " tell VIM to always put a status line in, even
                                "    if there is only one window
"set cmdheight=2                 " use a status bar that is 2 rows high
" }}}


" Vim behaviour {{{
"set hidden                      " hide buffers instead of closing them this
                                "    means that the current buffer can be put
                                "    to background without being written; and
                                "    that marks and undo history are preserved
set switchbuf=useopen           " reveal already opened files from the
                                " quickfix window instead of opening new
                                " buffers
set history=1000                " remember more commands and search history
set undolevels=1000             " use many muchos levels of undo
if v:version >= 730
  set undofile                  " keep a persistent backup file
  set undodir=~/.vim/.undo,~/tmp,/tmp
endif
set nobackup                    " do not keep backup files, it's 70's style cluttering
set noswapfile                  " do not write annoying intermediate swap files,
set directory=~/.vim/.tmp,~/tmp,/tmp
                                " store swap files in one of these directories
                                "    (in case swapfile is ever turned on)
set viminfo='20,\"80            " read/write a .viminfo file, don't store more
                                "    than 80 lines of registers
set wildmenu                    " make tab completion for files/buffers act like bash
set wildmode=list:full          " show a list when pressing tab and complete
                                "    first full match
set wildignore=*.swp,*.bak,*.pyc,*.class
"set title                       " change the terminal's title
set visualbell                  " don't beep
set noerrorbells                " don't beep
set showcmd                     " show (partial) command in the last line of the screen
                                "    this also shows visual selection info
set nomodeline                  " disable mode lines (security measure)
"set ttyfast                     " always use a fast terminal
set cursorline                  " underline the current line, for quick orientation

" Tame the quickfix window (open/close using \F)
nmap <silent> <leader>F :QFix<CR>

command! -bang -nargs=? QFix call QFixToggle(<bang>0)
function! QFixToggle(forced)
  if exists("g:qfix_win") && a:forced == 0
    cclose
    unlet g:qfix_win
  else
    copen 10
    let g:qfix_win = bufnr("$")
  endif
endfunction

set ruler   " Show the line and column number of the cursor position.
" }}}

syntax enable

" Editor appearance {{{
if has("gui_running")
  set guifont=Source\ Code\ Pro:h12
  let g:solarized_termcolors=256
  let g:solarized_bold=1
  let g:solarized_underline=1
  let g:solarized_italic=1
  colorscheme solarized

  " Remove toolbar, left scrollbar and right scrollbar
  set guioptions-=T
"  set guioptions-=l
"  set guioptions-=L
"  set guioptions-=r
"  set guioptions-=R

  set lines=50
  set columns=120

  " Screen recording mode
  function! ScreenRecordMode()
    set columns=80
    set guifont=Source\ Code\ Pro:h14
    set cmdheight=1
  endfunction
  command! -bang -nargs=0 ScreenRecordMode call ScreenRecordMode()
else
  set background=dark
  let g:solarized_termcolors=256
  colorscheme solarized
endif
" }}}

" Edit the vimrc file
nnoremap <silent> <leader>ev :e $MYVIMRC<CR>
nnoremap <silent> <leader>sv :so $MYVIMRC<CR>

" Clears the search register using the ENTER key.
nnoremap <CR> :nohlsearch<CR><CR>

" Jump to matching pairs easily, with Tab
nnoremap <Tab> %
vnoremap <Tab> %

" Folding
nnoremap <Space> za
vnoremap <Space> za

" Strip all trailing whitespace from a file, using \w
nnoremap <leader>W :%s/\s\+$//<CR>:let @/=''<CR>

" Set the current working directory to the file being edited and print.
map <leader>cd :cd %:p:h<CR>:pwd<CR>

" Toggle spell checking
map <F6> :setlocal spell! spelllang=en_us<CR>

" Toggle line number display
map <F5> :set number!<CR>

" Toggle display of the tagbar
nnoremap <silent> <F9> :TagbarToggle<CR>

" NERDTree settings {{{
" Put focus to the NERD Tree with F3 (tricked by quickly closing it and
" immediately showing it again, since there is no :NERDTreeFocus command)
"nmap <leader>n :NERDTreeClose<CR>:NERDTreeToggle<CR>
"nmap <leader>m :NERDTreeClose<CR>:NERDTreeFind<CR>
"nmap <leader>N :NERDTreeClose<CR>

" Store the bookmarks file
let NERDTreeBookmarksFile=expand("$HOME/.vim/NERDTreeBookmarks")

" Show the bookmarks table on startup
let NERDTreeShowBookmarks=1

" Show hidden files, too
let NERDTreeShowFiles=1
let NERDTreeShowHidden=1

" Quit on opening files from the tree
"let NERDTreeQuitOnOpen=1

" Highlight the selected entry in the tree
let NERDTreeHighlightCursorline=1

" Use a single click to fold/unfold directories and a double click to open
" files
let NERDTreeMouseMode=2

" Don't display these kinds of files
let NERDTreeIgnore=[ '\.pyc$', '\.pyo$', '\.py\$class$', '\.obj$',
            \ '\.o$', '\.so$', '\.egg$', '^\.git$', '\.DS_Store' ]

" }}}


" Unite settings {{{
nnoremap <leader>f :<C-u>Unite -start-insert file<CR>
nnoremap <leader>ls :<C-u>Unite -start-insert buffer<CR>

call unite#filters#matcher_default#use(['matcher_fuzzy'])
call unite#filters#sorter_default#use(['sorter_rank'])
nnoremap <leader>r :<C-u>Unite -start-insert file_rec<CR>

" Unite plugin and the_platinum_searcher integration (requires vimproc)
nnoremap <silent> <leader>g :<C-u>Unite grep:. -buffer-name=search-buffer<CR>
nnoremap <silent> <leader>gr :<C-u>Unite grep:.:-e: -buffer-name=search-buffer<CR>
if executable('pt')
  " Use pt in unite grep source.
  " https://github.com/monochromegane/the_platinum_searcher
  let g:unite_source_grep_command = 'pt'
  let g:unite_source_grep_default_opts = '--nogroup --nocolor'
  let g:unite_source_grep_recursive_opt = ''
  let g:unite_source_grep_encoding = 'utf-8'
endif
" }}}

" vim-go settings {{{
let g:go_fmt_command = "goimports"
let g:go_dispatch_enabled = 1
" Show a list of interfaces which is implemented by the type under your cursor
au FileType go nmap <Leader>s <Plug>(go-implements)
" Show type info for the word under your cursor
au FileType go nmap <Leader>i <Plug>(go-info)
" Open the relevant Godoc for the word under the cursor
au FileType go nmap <Leader>gd <Plug>(go-doc)
" Rename the identifier under the cursor to a new name
au FileType go nmap <Leader>e <Plug>(go-rename)
"}}}

" syntastic settings {{{
" Improve saving and opening Go files.
let g:syntastic_go_checkers = ['golint', 'govet', 'errcheck']
let g:syntastic_mode_map = { 'mode': 'active', 'passive_filetypes': ['go'] }
"}}}

" neocomplete settings {{{
" Disable AutoComplPop.
let g:acp_enableAtStartup = 0
" Use neocomplete.
let g:neocomplete#enable_at_startup = 1
" Use smartcase.
let g:neocomplete#enable_smart_case = 1
" AutoComplPop like behavior.
let g:neocomplete#enable_auto_select = 1
" Disable auto popup.
let g:neocomplete#disable_auto_complete = 0
"}}}

" incsearch settings {{{
" Basic usage
map /  <Plug>(incsearch-forward)
map ?  <Plug>(incsearch-backward)
map g/ <Plug>(incsearch-stay)
"}}}

set number
autocmd VimEnter * NERDTree
autocmd VimEnter * wincmd p
autocmd VimEnter * TagbarOpen
